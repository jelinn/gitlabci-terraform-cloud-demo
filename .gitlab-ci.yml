stages:
  - create_workspace
  - setup_workspace
# Uncomment get_credentials to fetch temporary credentials from Vault
  - get_credentials
  - run

before_script:
  - >
    export workspace_id=$(curl -s --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces/${TFC_WORKSPACE}" | jq -r .data.id)
  - echo "Workspace ID is ${workspace_id}"

# Uncomment the get_credentials job to fetch temporary credentials from Vault
get_credentials:
  stage: get_credentials
  script:
  - echo "Checking if we can reach Vault @ ${VAULT_ADDR}"
  - >
    curl ${VAULT_ADDR}/v1/sys/health
  - echo "Looking up token"
  - >
    curl --header "X-Vault-Token: ${VAULT_TOKEN}" ${VAULT_ADDR}/v1/auth/token/lookup-self
  - echo "Getting new GCP credentials @ ${VAULT_ADDR}/v1/${SECRETS_PATH}"
  - >
    curl --header "X-Vault-Token: ${VAULT_TOKEN}" ${VAULT_ADDR}/v1/${SECRETS_PATH} | jq -r .data.private_key_data | base64 --decode > temp_creds
  - export GOOGLE_CREDENTIALS=$(tr '\n' ' ' < temp_creds | sed -e 's/\"/\\\\"/g' -e 's/\//\\\//g' -e 's/\\n/\\\\\\\\n/g')
  - rm -f temp_creds
  - sed -e "s/my-key/GOOGLE_CREDENTIALS/" -e "s/my-hcl/false/" -e "s/my-value/${GOOGLE_CREDENTIALS}/" -e "s/my-category/env/" -e "s/my-sensitive/true/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable.json
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @variable.json "https://${TFC_ADDR}/api/v2/vars"
  tags:
    - curl

create_workspace:
  stage: create_workspace
  script:
  - sed "s/placeholder/${TFC_WORKSPACE}/" < api_templates/workspace.json.template > workspace.json
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --request POST --data @workspace.json "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces" > workspace_result.txt
  - export workspace_id=$(cat workspace_result.txt | jq -r .data.id)
  - echo "Workspace ID is ${workspace_id}"
  - >
    export workspace_id=$(curl -s --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces/${TFC_WORKSPACE}" | jq -r .data.id)
  - echo "Workspace ID is ${workspace_id}"
  tags:
    - curl

workspace_vars:
  stage: setup_workspace
  script:
# Delete existing variables
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/vars?filter%5Borganization%5D%5Bname%5D=${TFC_ORG}&filter%5Bworkspace%5D%5Bname%5D=${TFC_WORKSPACE}" > vars.json
  - x=$(cat vars.json | jq -r ".data[].id" | wc -l | awk '{print $1}')
  - >
    for (( i=0; i<$x; i++ ))
    do
       curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --request DELETE https://${TFC_ADDR}/api/v2/vars/$(cat vars.json | jq -r ".data[$i].id")
    done
# Environment variables
  - sed -e "s/my-key/CONFIRM_DESTROY/" -e "s/my-hcl/false/" -e "s/my-value/1/" -e "s/my-category/env/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable0.json
# Terraform variables  
  - sed -e "s/my-key/gcp_region/" -e "s/my-hcl/false/" -e "s/my-value/${gcp_region}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable1.json
  - sed -e "s/my-key/gcp_project/" -e "s/my-hcl/false/" -e "s/my-value/${gcp_project}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable2.json
  - if [ -z "$machine_type" ]; then machine_type="n1-standard-1"; fi
  - sed -e "s/my-key/machine_type/" -e "s/my-hcl/false/" -e "s/my-value/${machine_type}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable3.json
  - >
    for (( i=0; i<4; i++ ))
    do
      curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @variable$i.json "https://${TFC_ADDR}/api/v2/vars"
    done
  - >
    if [ ! -z "$GOOGLE_CREDENTIALS" ]; then
      sed -e "s/my-key/GOOGLE_CREDENTIALS/" -e "s/my-hcl/false/" -e "s/my-value/${GOOGLE_CREDENTIALS}/" -e "s/my-category/env/" -e "s/my-sensitive/true/" -e "s/my-workspace-id/${workspace_id}/" < api_templates/variable.json.template  > variable.json;
      curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @variable.json "https://${TFC_ADDR}/api/v2/vars"; fi
  retry:
    max: 1
    when: script_failure
  tags:
    - curl

config_version:
  stage: setup_workspace
  script:
  - tar -cvf myconfig.tar *.tf scripts/webapp.sh
  - gzip myconfig.tar
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @api_templates/configversion.json.template "https://${TFC_ADDR}/api/v2/workspaces/${workspace_id}/configuration-versions" > configuration_version.txt
  - cat configuration_version.txt
  - export config_version_id=$(cat configuration_version.txt | jq -r .data.id)
  - export upload_url=$(cat configuration_version.txt | jq -r '.["data"]["attributes"]["upload-url"]')
  - echo "Config Version ID is ${config_version_id}"
  - echo "Upload URL is ${upload_url}"
  - >
    curl --request PUT -F 'data=@myconfig.tar.gz' "${upload_url}"
  retry:
    max: 1
    when: script_failure
  tags:
    - curl

apply:
  stage: run
  script:
  - sed "s/my-workspace-id/${workspace_id}/" < api_templates/run.json.template  > run.json
  - cat run.json
  - >
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @run.json https://${TFC_ADDR}/api/v2/runs > run_result.txt
  - cat run_result.txt
  - run_id=$(cat run_result.txt | jq -r .data.id)
  - echo "Run ID is ${run_id}"
  - >
    curl --header "Authorization: Bearer $TFC_TOKEN" --header "Content-Type: application/vnd.api+json" https://${TFC_ADDR}/api/v2/runs/${run_id} > result.txt
  - cat result.txt
  - result=$(cat result.txt | jq -r .data.attributes.status)
  - echo "Run result is ${result}. View this Run in TFC UI:"
  - echo "https://${TFC_ADDR}/app/${TFC_ORG}/workspaces/${TFC_WORKSPACE}/runs/${run_id}"
  tags:
    - curl
